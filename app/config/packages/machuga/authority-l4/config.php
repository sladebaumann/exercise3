<?php

return array(

    'initialize' => function($authority) {
        $user = $authority->getCurrentUser();

        $authority->addAlias('superuser', array('create', 'read', 'update', 'delete'));
        $authority->addAlias('regularuser', array('create','read', 'update'));
        $authority->addAlias('guest', array('read'));

        if ($user->hasRole('superuser')) {
            $authority->allow('superuser', 'all');
        }
        if ($user->hasRole('regularuser')) {
            $authority->allow('regularuser', 'all');
        }
        if ($user->hasRole('guest')) {
            $authority->deny('guest', 'all');
        }
        // loop through each of the users permissions, and create rules
        foreach($user->permissions as $perm) {
            if($perm->type == 'allow') {
                $authority->allow($perm->action, $perm->resource);
            } else {
                $authority->deny($perm->action, $perm->resource);
            }
        }
    }
);