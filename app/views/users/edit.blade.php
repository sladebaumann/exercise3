<!DOCTYPE html>
<html>
<head>
	<title>Look! I'm CRUDding</title>
	<link rel="stylesheet"

href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('users') }}">User Page</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('posts') }}">View All Posts</a></li>
		<li><a href="{{ URL::to('posts/create') }}">Create a Post</a>
		<li><a href="{{ URL::to('users') }}">View All Users</a></li>
        <li><a href="{{ URL::to('users/create') }}">Create a User</a>
		<!-- LOGOUT BUTTON -->
        <li><a href="{{ URL::to('logout') }}">Logout</a></li>
	</ul>
</nav>

<h1>Edit {{ $user->userName }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($user, array('route' => array('users.update', $user->id), 'method' =>

          'PUT')) }}

	<div class="form-group">
		{{ Form::label('userName', 'Name') }}
		{{ Form::text('userName', null, array('class' => 'form-control')) }}
	</div>

	<div class="form-group">
		{{ Form::label('email', 'Email') }}
		{{ Form::email('email', null, array('class' => 'form-control')) }}
	</div>

	<div class="form-group">
		{{ Form::label('password', 'Password') }}
		{{ Form::password('password', null, array('class' => 'form-control')) }}
	</div>

	{{ Form::submit('Edit the User!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>