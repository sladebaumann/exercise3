<!DOCTYPE html>
<html>
<head>
	<title>Basic Blog - User</title>
	<link rel="stylesheet"

href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('users') }}">User Page</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('posts') }}">View All Posts</a></li>
		<li><a href="{{ URL::to('posts/create') }}">Create a Post</a>
		<li><a href="{{ URL::to('users') }}">View All Users</a></li>
        <li><a href="{{ URL::to('users/create') }}">Create a User</a>
		<!-- LOGOUT BUTTON -->
        <li><a href="{{ URL::to('logout') }}">Logout</a></li>
	</ul>
</nav>

<h1>Showing {{ $user->userName }}</h1>

	<div class="jumbotron text-center">
		<h2>{{ $user->userName }}</h2>
		<p>
			<strong>Email:</strong> {{ $user->email }}
		</p>
	</div>


</div>
</body>
</html>