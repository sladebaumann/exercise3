<!DOCTYPE html>
<html>
<head>
	<title>Basic Blog - Contact User</title>
	<link rel="stylesheet"

href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('users') }}">User Page</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('posts') }}">View All Posts</a></li>
		<li><a href="{{ URL::to('posts/create') }}">Create a Post</a>
		<li><a href="{{ URL::to('users') }}">View All Users</a></li>
        <li><a href="{{ URL::to('users/create') }}">Create a User</a>
		<!-- LOGOUT BUTTON -->
		<li><a href="{{ URL::to('logout') }}">Logout</a></li>
	</ul>
</nav>

<h1>Email</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

    {{ Form::open(array('route' => array('contact', $user->id))) }}

        <div class="form-group">
    		{{ Form::label('title', 'Title') }}
    		{{ Form::text('title', null, array('class' => 'form-control')) }}
    	</div>

    	<div class="form-group">
            {{ Form::label('contents', 'Contents') }}
            {{ Form::textarea('contents', null, array('class' => 'form-control')) }}
        </div>

    	{{ Form::submit('Send Email', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}






</div>
</body>
</html>