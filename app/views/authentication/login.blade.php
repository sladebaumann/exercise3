<!doctype html>
<html>
    <head>
        <title>Basic Blog - Login</title>
        <link rel="stylesheet"

        href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">

        <nav class="navbar navbar-inverse">
        	<div class="navbar-header">
        		<a class="navbar-brand" href="{{ URL::to('login') }}">Login Page</a>
        		<a class="navbar-brand" href="{{ URL::to('posts') }}">Posts Page</a>
        	</div>
        </nav>

        {{ Form::open(array('url' => 'login')) }}
        <h1>Login</h1>

        <!-- if there are login errors, show them here -->
        <p>
            {{ $errors->first('email') }}
            {{ $errors->first('password') }}
        </p>

        <p>
            {{ Form::label('email', 'Email') }}
            {{ Form::text('email', Input::old('email'), array('placeholder' => 'user@host.com')) }}
        </p>

        <p>
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password') }}
        </p>

        <p>{{ Form::submit('Submit!') }}</p>
        {{ Form::close() }}

        <a class="btn btn-small btn-success" href="{{ URL::to

        ('password') }}">Forgot Password?</a>



    </div>
    </body>
    </html>