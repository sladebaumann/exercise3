<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// get all the nerds
        $users = User::all();

        // load the view and pass the nerds
        return View::make('users.index')
            ->with('users', $users);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        //for testing purposes
        $users = User::all();

        if (Auth::guest()) {
            return View::make('users.index')
                ->with('users', $users);
        } else {
            if (Authority::can('create', 'User')) {
                return View::make('users.create');
            } else {
                return View::make('users.index')
                    ->with('users', $users);
            }
        }
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'userName'       => 'required',
            'email'      => 'required|email',
            'password' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('users/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            //save hashed version of password to a variable
            $password = Input::get('password');

            // store
            $user = new User;
            $user->userName       = Input::get('userName');
            $user->email      = Input::get('email');
            $user->password = Hash::make($password);
            $user->save();

            // redirect
            Session::flash('message', 'Successfully created user!');
            return Redirect::to('users');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	    // get the nerd
        $user = User::find($id);

        // show the view and pass the nerd to it
        return View::make('users.show')
            ->with('user', $user);

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

        //for testing purposes
        $users = User::all();

        // get the nerd
        $user = User::find($id);

        if (Auth::guest()) {
            return View::make('users.index')
                ->with('users', $users);
        } else {
            if (Authority::can('update', 'User')) {
                return View::make('users.edit')
                    ->with('user', $user);
            } else {
                return View::make('users.index')
                    ->with('users', $users);
            }
        }
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'userName'       => 'required',
            'email'      => 'required|email',
            'password' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('users/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            //save plaintext version of password to a variable
            $password = Input::get('password');

            // store
            $user = User::find($id);
            $user->userName       = Input::get('userName');
            $user->email      = Input::get('email');
            $user->password = Hash::make($password);
            $user->save();

            // redirect
            Session::flash('message', 'Successfully updated User!');
            return Redirect::to('users');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
    {

        //for testing purposes
        $users = User::all();

        // find current post
        $user = User::find($id);

        if (Auth::guest()) {
            return View::make('users.index')
                ->with('users', $users);
        } else {
            if (Authority::can('delete', 'User')) {
                $user->delete();
                // redirect
                Session::flash('message', 'Successfully deleted the user!');
                return Redirect::to('users');
            } else {
                return View::make('users.index')
                    ->with('users', $users);
            }
        }
    }

    public function contact($id){

        // get the nerd
        $user = User::find($id);

        // show the view and pass the nerd to it
        return View::make('users.contact')
            ->with('user', $user);
    }

    public function contact_user($id){

        $body = Input::get('contents');
        $title = Input::get('title');
        $user = User::find($id);

        Mail::send([], [], function ($message) use ($user, $title, $body)
        {
            $message->to($user->email,'User')
                ->subject($title)
                ->setBody($body);
        });


        Session::flash('message', 'Successfully sent email to ' . $user->userName . '.');
        return Redirect::to('users');
    }
}
