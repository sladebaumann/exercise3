<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

    public function showLogin()
    {
        // show the form
        return View::make('authentication.login');
    }

    public function doLogin()
    {
// validate the info, create rules for the inputs
        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required' // within ' should be |alphaNum|min:3   password can only be alphanumeric and has to be greater than 3 characters
        );

// run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

// if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'email'     => Input::get('email'),
                'password'  => Input::get('password')
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {

                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)
                return Redirect::to('posts');

            } else {

                // validation not successful, send back to form
                return Redirect::to('login')->with('message', 'Validation was unsuccessful. Make sure your email/password is correct');

            }

        }
    }

    public function doLogout()
    {
        Auth::logout(); // log the user out of our application
        return Redirect::to('login')->with('message', 'Successfully logged out.');
    }


    public function get_export_users()
    {
        $table = ExportTable::all();
        $file = fopen('Users.csv', 'w');
            foreach ($table as $row) {
                fputcsv($file, $row->toArray());
            }
        fclose($file);

        Session::flash('message', 'Successfully exported users table to public/Users.csv!');
        return Redirect::to('users');
    }

    public function get_export_posts()
    {
        $table = ExportTablePosts::all();
        $file = fopen('Posts.csv', 'w');
            foreach ($table as $row) {
                fputcsv($file, $row->toArray());
            }
        fclose($file);

        Session::flash('message', 'Successfully exported posts table to public/Posts.csv!');
        return Redirect::to('posts');
    }
}
