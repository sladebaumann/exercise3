<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        // get all the posts
        $posts = Post::all();

        // load the view and pass the nerds
        return View::make('posts.index')
            ->with('posts', $posts);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        //for testing purposes
        $posts = Post::all();

        if (Auth::guest()) {
            return View::make('posts.index')
                ->with('posts', $posts);
        } else {
            if (Authority::can('create', 'Post')) {
                return View::make('posts.create');
            } else {
                return View::make('posts.index')
                    ->with('posts', $posts);
            }
        }
        // load the create form (app/views/nerds/create.blade.php)
        //return View::make('posts.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $user_id = Auth::user()->id;
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'post'      => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('posts/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $post = new Post;
            $post->title       = Input::get('title');
            $post->post      = Input::get('post');
            $post->user_id  = $user_id;
            $post->save();

            // redirect
            Session::flash('message', 'Successfully created post!');
            return Redirect::to('posts');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the nerd
        $post = Post::find($id);

        // show the view and pass the nerd to it
        return View::make('posts.show')
            ->with('post', $post);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        //for testing purposes
        $posts = Post::all();

        // get the nerd
        $post = Post::find($id);

        if (Auth::guest()) {
            return View::make('posts.index')
                ->with('posts', $posts);
        } else {
            if (Authority::can('update', 'Post')) {
                return View::make('posts.edit')
                    ->with('post', $post);
            } else {
                return View::make('posts.index')
                    ->with('posts', $posts);
            }
        }
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'post'      => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('posts/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $post = Post::find($id);
            $post->title      = Input::get('title');
            $post->post      = Input::get('post');
            $post->save();

            // redirect
            Session::flash('message', 'Successfully updated post!');
            return Redirect::to('posts');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

        //for testing purposes
        $posts = Post::all();

        // find current post
        $post = Post::find($id);

        if (Auth::guest()) {
            return View::make('posts.index')
                ->with('posts', $posts);
        } else {
            if (Authority::can('delete', 'Post')) {
                $post->delete();
                // redirect
                Session::flash('message', 'Successfully deleted the post!');
                return Redirect::to('posts');
            } else {
                return View::make('posts.index')
                    ->with('posts', $posts);
            }
        }
    }


}
